# Jasvascript - Cursos e Estudos
![Javascript Logo](jslogo.jpg)
#### Algoritmos e Lógica de Programação com Javascript aplicado ao desenvolvimento.

Os estudos estão seguindo orientações dos seguintes recursos:

1-> livro 'Introdução a lógica com linguagem Javascript' (Ed. Casa do Código);  
2-> Curso online Javascript 'FreeCodeCamp - 2018'.  
3-> Cursos online Javascript 'Udacity - 2018'.

> Estudar, constrói um mundo de possibilidades! (autor desconhecido)
