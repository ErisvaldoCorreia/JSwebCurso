// Novas Features no EcmaScript 6

/*
  ECMAScript é uma versão padronizada do JavaScript com o objetivo de unificar
  as especificações e características do idioma. Como todos os principais
  navegadores e tempos de execução de JavaScript seguem essa especificação,
  o termo ECMAScript é intercambiável com o termo JavaScript.
  A versão padronizada mais recente é chamada ECMAScript 6 (ES6), lançada em
  2015. Esta nova versão do idioma adiciona alguns recursos poderosos que serão
  abordados nesta seção de desafios, incluindo:
      Arrow functions
      Classes
      Modules
      Promises
      Generators
      let and const
*/

// let no lugar de var!
// com var podemos declarar uma variavel com o mesmo nome em varios pontos do codigo

var nome = 'Erisvaldo';
console.log(nome);

var nome = 'Junior';
console.log(nome);

// No EcmaScript6 foi introduzido um novo formato de declaração de variavel. O let
// Ele só permite que a variavel seja declarada uma unica vez!

let idade = 15;
console.log(idade);

// a linha abaixo gera um erro! Remova os comentarios para ver!
//let idade = 18; Isso causaria um erro no código
// podemos reatribuir o valor, sem uma nova declaração.
idade = 18;
console.log(idade);

/*
  Observe o "uso estrito". Isso habilita o Modo Estrito, que detecta erros
  comuns de codificação e ações "inseguras". Por exemplo:
*/
'use strict';
let printNumTwo1;
for (let i = 0; i < 3; i++) {
  if (i === 2) {
    printNumTwo1 = function() {
      return i;
    };
  }
}
console.log(printNumTwo1());
