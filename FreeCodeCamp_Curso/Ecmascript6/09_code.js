// Usando o operador rest nos argumentos de funções
// com o uso do rest, podemos informar um conjunto de argumentos


// exemplo função:
const sum = (function() {
    "use strict";
    return function sum(x, y, z) {
      const args = [ x, y, z ];
      return args.reduce((a, b) => a + b, 0);
    };
})();

console.log(sum(1, 2, 3));

// rest operator nos provê acesso as funções map(), filter() e reduce()

// função reescrita com uso do rest operator
const sum2 = (function(){
    "use scrict";
    return function sum2(...n) {
        return n.reduce((a, b) => a + b, 0);
    }
})();

console.log(sum2(1, 2, 3));
console.log(sum2(3, 3, 3, 3, 5, 5, 5));

// podemos trabalhar com o operador para fazer concats de arrays
let arr = [1,2,3,4];
let arr2 = [0, ...arr, 5,6];
console.log(arr2);