// Arrows Functions

/*
  Em JavaScript, geralmente não precisamos nomear nossas funções, especialmente
  ao passar uma função como um argumento para outra função. Em vez disso,
  criamos funções inline. Não precisamos nomear essas funções porque não as
  reutilizamos em nenhum outro lugar.

  Para conseguir isso, geralmente usamos a seguinte sintaxe:
*/

const myFunc = function() {
  const myVar = "value";
  return myVar;
}

//No ES6 temos a introdução das chamadas Arrows Functions para simplificar.
const myFunc = () => {
  const myVar = "value";
  return myVar;
}

/*
  Quando não há corpo da função e apenas um valor de retorno, a sintaxe da
  função de seta permite omitir o retorno da palavra-chave, bem como os colchetes
  ao redor do código. Isso ajuda a simplificar funções menores em instruções
  de uma linha:
*/

const myFunc = () => "value";

// Outro exemplo de uso:
const magic = () => new Date();
