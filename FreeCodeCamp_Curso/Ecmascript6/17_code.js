// Métodos Getters e Setters

// usamos os getters para pegar um valor de um objeto
// usamos os setters para atribuir um valor ao objeto
class Book {
    constructor(author) {
      this._author = author;
    }
    
    // getter
    get writer(){
      return this._author;
    }
    
    // setter
    set writer(updatedAuthor){
      this._author = updatedAuthor;
    }
}

// criamos o objeto livro lol (new)
const lol = new Book('anonymous');
// pegamos o valor contido em _author (get)
console.log(lol.writer);
// redefinimos o valor (set)
lol.writer = 'wut';
// exibimos o novo valor. (get) 
console.log(lol.writer);
