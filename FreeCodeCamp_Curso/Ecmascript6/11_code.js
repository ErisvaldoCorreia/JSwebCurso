// destructuring assignment

// podemos passar a desestruturação para assinalar objetos
function getLength(str) {
    
    // str passa a função len o tamanho da string
    // que retorna um valor inteiro.
    const { length : len } = str;  
    return len;
  
}
console.log(getLength('Erisvaldo'));



// podemos assinalar a desestruturação também em objetos aninhados
const alpha = {
    start: { x: 5, y: 6},
    end: { x: 6, y: -9 }
  };
const { start : { x: startX, y: startY }} = alpha;
console.log(startX, startY);



// Exemplo com passagem de função
const LOCAL_FORECAST = {
    today: { min: 72, max: 83 },
    tomorrow: { min: 73.3, max: 84.6 }
};
function getMaxOfTmrw(forecast) {
    const { tomorrow :{ max: maxOfTomorrow}} = forecast;
    return maxOfTomorrow;
}
console.log(getMaxOfTmrw(LOCAL_FORECAST));



// Em vetores, os indices são atribuidos em sequencia.
// podemos usar virgulas com espaços para coletar indices especificos
const [a, b] = [1, 2, 3, 4, 5, 6];
console.log(a, b); // 1, 2

const [x, y,,, z] = [1, 2, 3, 4, 5, 6];
console.log(x, y, z); // 1, 2, 5



// Podemos fazer uso da desestruturação para inversão de valores
let num1 = 2;
let num2 = 5;
[num2, num1] = [num1, num2];
console.log(num1, num2);