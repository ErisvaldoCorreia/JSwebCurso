// export e import default

// Quando criamos um modulo, podemos conter uma função
// padrão. Para isso exportamos a mesma, e onde precisarmos
// importamos a mesma para uso.


// Arquivo - myFunction.js
const somar = (a, b) => {
    return a + b;
};
export default somar;


// Arquivo - usandoFunction.js
import somar from './myFunction';
somar(5,4);

/* Foram indicados como dois arquivos distintos */
