// Para que um objeto não seja alterado mesmo com a atribuição de const
// Usamos o Object.freeze();

function freezeObj() {
  "use strict";

  const MATH_CONSTANTS = {
    PI: 3.14
  };

  // congelando o MATH_CONSTANTS para que não seja alterado!
  Object.freeze(MATH_CONSTANTS);

  // comentado o bloco try veremos a execução!
  try {
    MATH_CONSTANTS.PI = 99;
  } catch( ex ) {
    // retorna a exceção de erros!
    console.log(ex);
  }

  return MATH_CONSTANTS.PI;
}

// chamada da função
const PI = freezeObj();
console.log(PI);
