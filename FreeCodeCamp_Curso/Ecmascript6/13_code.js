// Templates Literals.

// Com o ES6 ficou muito mais facil escrever saidas
// podemos usar as templates literals para composição.

// Saida antiga:
let nome = 'Joao';
let idade = 38;

console.log('Bem vindo ' + nome + '. \nSua idade é ' + idade);

// com as templates literals usamos `strings e ${variaveis}` 
// tudo dentro de acentos craseados.
// Podemos também simplesmente quebrar a linha de saida
// sem uso do operador de escape linha \n
console.log(`Bem vindo ${nome}. 
Sua idade é ${idade}`);

// Podemos usar arrays e objetos também!
const arr = [1,2,3,4];
console.log(`Posição 0 = ${arr[0]}`);
const obj = {
    nome: 'Joao',
    idade: 38
};
console.log(`Idade de ${obj.nome} é ${obj.idade}`);
