// Uso do Class na construção de objetos

/*  
    O ES6 fornece uma nova sintaxe para ajudar a criar objetos,
    usando a palavras-chave Class.

    Isso deve ser notado, que a sintaxe da class é apenas uma 
    sintaxe, e não uma implementação baseada em classes completa 
    do paradigma orientado a objetos.
*/

// Padrão antigo
var SpaceShuttle = function(targetPlanet){
    this.targetPlanet = targetPlanet;
}
var zeus = new SpaceShuttle('Jupiter');


// Padrão ES6
class Space {
    constructor(targetPlanet){
      this.targetPlanet = targetPlanet;
    }
}
const planet = new Space('Jupiter');

/* 
    Observe que a palavra-chave class declara uma nova função, 
    e um construtor foi adicionado, o qual seria invocado quando 
    new é chamado - para criar um novo objeto.
*/