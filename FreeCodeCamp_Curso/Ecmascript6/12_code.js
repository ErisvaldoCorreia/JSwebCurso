// usando o rest operator para reassinalar vetores.

// perceba que ao usar o rest operator, a variavel
// arr recebeu o restante do array.
const [a, b, ...arr] = [1, 2, 3, 4, 5, 7];
console.log(a, b); // 1, 2
console.log(arr); // [3, 4, 5, 7]


// Nesse exemplo, usamos o rest operator seguido de
// virgulas vazias para indicar a omissão dos primeiros
// elementos do array na desestruturização.
// criamos assim um novo vetor chamado cont.
const source = [1,2,3,4,5,6,7,8,9];
const [, , ...cont] = source;
console.log(cont);


// Exemplo com passagem de valores para função
const codes = [1,2,3,4,5];

function getLasts(a) {
    const [, , , ...last] = a;
    return last;
};
console.log(getLasts(codes));