// O ecmascript6 permite a criação de valores default como parametros de funções
// podemos definir um valor caso a chamada seja sem argumento.

// exemplo:
function greeting(name = "Anonymous") {
  return "Hello " + name;
}
console.log(greeting("John"));
// Saida do terminal: Hello John
console.log(greeting());
// Saida do terminal: Hello Anonymous


// Outro exemplo para Fixação.
// Imagine uma função que soma dois valores. Caso um não seja informado o padrão
// deverá ser valor 1!

const increment = (function() {
  "use strict";
  return function increment(number, value = 1) {
    return number + value;
  };
})();

console.log(increment(5, 2));
console.log(increment(5));

// Repare que não estamos usando nesse exemplo, notação de arrows functions
