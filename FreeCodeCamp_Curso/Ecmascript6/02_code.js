// Introdução da palavra chave Const

// diferentemente de let e var, Const declara uma variavel imutavel. Ou seja,
// uma vez inicializado seu valor, o mesmo permanecerá uma constante.
// diferentemente do que acontece com o let, no qual não podemos declarar novamente
// a variavel com o mesmo nome, no const, não podemos alterar seus valores.

const NAME = 'Junior';
console.log(NAME);

// a linha a seguir cria um erro:
//NAME = 'Marcos';

// Observe que usamos a nome da variavel todo em letras maiusculas. Isso se deve
// a uma convenção de boas praticas na qual usamos a uppercase em nome de constantes.

const PI = 3.1415;
let valor = 3.1415;
console.log(valor);
valor = 2.14154;
console.log(valor);

// Exemplo de aplicação de const e let
function printManyTimes(str) {
  "use strict";
  const SENTENCE = str + " is cool!";
  for(let i = 0; i < str.length; i+=2) {
    console.log(SENTENCE);
  }
}

printManyTimes("freeCodeCamp");
