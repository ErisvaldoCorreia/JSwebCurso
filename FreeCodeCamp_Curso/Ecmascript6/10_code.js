// spread operator. (continuação do rest)

// nos permite expandir matrizes e expressões onde varios
// parametros ou elementos são esperados

// exemplo antigo:
var arr = [6, 89, 3, 45];
var maximus = Math.max.apply(null, arr);
console.log(maximus);


// usando o operator.
const arr2 = [6, 89, 3, 45];
const maximus2 = Math.max(...arr2);
console.log(maximus2);
