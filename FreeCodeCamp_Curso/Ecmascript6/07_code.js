// usando funções superiores junto das arrows functions

// Veja o exemplo a seguir:
const realNumberArray = [4, 5.6, -9.8, 3.14, 42, 6, 8.34];
const squareList = (arr) => {
  "use strict";
  const squaredIntegers = arr;
  return squaredIntegers;
};

const squaredIntegers = squareList(realNumberArray);
console.log(squaredIntegers);

// Fazendo uso das funções superiores:
const squareList = (arr) => {
  "use strict";
  const squaredIntegers = arr.filter(
    (num) => num > 0 && num % parseInt(num) === 0 ).map( (num) => Math.pow(num, 2)
  );
  return squaredIntegers;
};

const squaredIntegers = squareList(realNumberArray);
console.log(squaredIntegers);

// Funções superiores: map(), filter(), reduce()
