// Escrevendo objetos literais

// com o ES6 podemos escrever um objeto de forma simples
// sem precisarmos usar as keys, uma vez passados por funções
const createPerson = (name, age, gender) => {
    return {
      name,
      age,
      gender
    };
};
console.log(createPerson("Zodiac Hasbro", 56, "male"));

// Outro exemplo
const gets = (a, b) => {
    return {a, b};
};

console.log(gets('Testando', 'Objetos'));
