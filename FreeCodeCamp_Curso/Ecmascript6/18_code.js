// Usando import e export

/*
    Nota: O uso do NodeJS ainda não suporta o import
    para tal é preciso usar uma transposição de codigo
    via babel
*/

// lugar de:
const modulo = require('nome_modulo');

// podemos usar:
import { funções } from 'nome_modulo';
// ainda podemos usar funções proprias
import { minhaFunção } from './meuModulo';

// usamos ./ para indicar o inicio do diretorio
// quando deixamos vazio, o js entende ser modulo padrão.


/*
    Essas linhas de código apenas ilustram o uso do import
    caso seja usado, teremos retornos de erros na execução.
*/

// export
// Para usarmos um modulo criado precisamos de um arquivo
// que contenha o termo chave Export - Exemplo.

const hello = (a) => {
    return console.log(`Olá, seja bem vindo ${a}`);
};

export { hello };

// Podemos querer exportar também variaveis.
export const a = 'holla mundo!';