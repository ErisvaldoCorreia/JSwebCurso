// Funções declarativas concisas dentro de objetos.

// Exemplo:
const person = {
    name: "Taylor",
    sayHello: function() {
      return `Hello! My name is ${this.name}.`;
    }
};
// Refatorado para ES6
const person2 = {
    name: "Taylor",
    sayHello2() {
      return `Hello! My name is ${this.name}.`;
    }
};

console.log(person.sayHello());
console.log(person2.sayHello2());

// Não há a necessidade do uso do termo Function na declarativa.
// Podemos também, se preciso incluir parametros na função.