// Podemos usar o termo const em um array. Isso torna o array imutavel, mas
// não seus indices!

const array = [1,2,3];
console.log(array);

// essa linha retorna um erro de execução!
array = [2,3,1];

// podemos modificar os elementos reatribuindo seus indices
array[2] = 45;
console.log(array);


// Exemplo de uma função alterando os valores!
const s = [5, 7, 2];
function editInPlace() {
  "use strict";

  s[0] = 2;
  s[1] = 5;
  s[2] = 7;
  // s = [2, 5, 7]; <- this is invalid
 return s

}
editInPlace();
