// Passando parametros nas arrows functions

// Exemplo de uma função tradicional
const myConcat = function(arr1, arr2) {
  "use strict";
  return arr1.concat(arr2);
};

console.log(myConcat([1, 2], [3, 4, 5]));

// função reescrita com as passagens de parametros e ES6
const myConcat = (arr1, arr2) => arr1.concat(arr2);

console.log(myConcat([1, 2], [3, 4, 5]));
