# Javascript
### Javascript Algorithms And Data Structures Certification (300 hours)
Implementação dos exemplos e estudos de Javascript

Alguns códigos foram copiados diretamente dos exercicios testes  
Outros, foram criados usando as bases estudadas em mais de uma lição.

### Histórico do Curso:   
1. Basic Javascript (107 lessons);  
2. Ecmascript6 (26 lessons);  
