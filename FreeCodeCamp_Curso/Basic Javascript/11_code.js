// Podemos usar um encademento de if junto ao else para compor varios testes
var nota = 6;

if (nota >= 7) {
  console.log('Parabéns. Você passou');
} else if (nota >= 5) {
  console.log('Infelizmente você esta de recuperação');
} else {
  console.log('Que pena. Voce foi reprovado');
}


// Operador ternario.
// Quando temos um unico teste com um retorno positivo ou negativo, podemos
// fazer uso do sistema de operador ternario.
// forma:  <teste> ? <resposta true> : <resposta false>
var teste = true;
var teste2 = false;

// teste é verdadeiro? se for receba sim senao (:) receba não
res1 = teste ? 'sim!' : 'não!';

// teste é verdadeiro? se for receba sim senao (:) receba não
res2 = teste2 ? 'sim!' : 'não!';

console.log(res1);  // saida sim!
console.log(res2);  // sainda não!
