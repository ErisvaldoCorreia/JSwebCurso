/*
  Na Ciência da Computação, uma fila é uma Estrutura de Dados abstrata, na qual
  os itens são mantidos em ordem. Novos itens podem ser adicionados na parte de
  trás da fila e itens antigos são retirados da frente da fila.

  Escreva uma função nextInLine que recebe um array (arr) e um número (item)
  como argumentos. Adicione o número ao final da matriz e remova o primeiro
  elemento da matriz. A função nextInLine deve então retornar o elemento que
  foi removido.
*/

function nextInLine(arr, item) {
  // adiciona um item ao final da matriz
  arr.push(item);
  // remove o primeiro item e adiciona na variavel
  var removed = arr.shift();
  // retorna o valor removido
  return removed;
}

var testArr = [1,2,3,4,5];

console.log("Before: " + testArr);
console.log(nextInLine(testArr, 6));
console.log("After: " + testArr);

// com este exercicio encerramos a parte de introdução as funções e arrays!
