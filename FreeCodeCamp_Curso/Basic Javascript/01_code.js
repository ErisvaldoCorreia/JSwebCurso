// exemplo de escape para uso das aspas na string
var sampleStr = "Alan said, \"Peter is learning JavaScript\".";
console.log(sampleStr)

// exemplo usando aspas simples na string e aspas duplas no corpo
var myStr = '<a href="http://www.example.com" target="_blank">Link</a>';
console.log(myStr);

// exemplos de strings
var meuNome = 'Erisvaldo';
var meuSobrnome = 'Correia';
console.log(meuNome + '' + meuSobrnome);


// Funções
// Podemos criar funções para facilitar a manutenção do nosso código bem como
// o reuso de trechos do código.

// essa função simplesmente imprime uma mensagem no console com a palavra recebida
function imprimeMensagem(palavra) {
  console.log(palavra);
}

//chamando a função
imprimeMensagem('Olá mundo');
// chamando a função novamente com outra frase
imprimeMensagem('Escrevendo outra frase!');
