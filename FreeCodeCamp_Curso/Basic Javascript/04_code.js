/*
  Com variáveis do tipo array JavaScript, podemos armazenar
  vários dados em um só lugar. Você inicia uma declaração de matriz
  com um colchete de abertura, termina com um colchete de fechamento
  e coloca uma vírgula entre cada entrada.
*/

// Example
var ourArray = ["John", 23];
var myArray = ['Erisvaldo', 32];
console.log(ourArray);

// podemos inclusive usar a notação dos colchetes passando indices para
// retornar uma posição especifica dentro do array.
console.log(myArray[0]); // primeiro indice
console.log(myArray[1]); // segundo indice


// podemos aninhar arrays dentro de arrays (chamada de matriz multidimencional)
var ourArray1 = [["the universe", 42], ["everything", 101010]];
var myArray1 = [["Nome", 44], [54, "sobrenome"]];
console.log(ourArray1);   // vendo arrays completos
console.log(myArray1[1]); // informando indice de um array interno

// informando indice de elemento dentro de um array interno
console.log(myArray1[1][1]);

// trocando valores dentro dos indices
var array = [50, 33, 25, 10];
console.log(array);
array[2] = 19;
// trocamos o numero 25 pelo numero 19
console.log(array);

// consolidação de acessos por indices em matriz multidimencional
var arr = [
  [1,2,3],
  [4,5,6],
  [7,8,9],
  [[10,11,12], 13, 14]
];
console.log(arr[3]); // equals [[10,11,12], 13, 14]
console.log(arr[3][0]); // equals [10,11,12]
console.log(arr[3][0][1]); // equals 11
