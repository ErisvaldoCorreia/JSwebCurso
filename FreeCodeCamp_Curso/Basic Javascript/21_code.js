// convertendo valores

// convertendo valores para inteiros
function numToInt(str) {
  return parseInt(str);
}

var tipo = typeof(numToInt('55'));
console.log(tipo);
// usamos a função parseInt para converter um valor para inteiro.
// podemos passar uma sequencia binaria e usar como argumento a base 2 para
// converter um valor binario para inteiro.

var bin = '101011';
var convertido = parseInt(bin, 2);
console.log(convertido);

// temos também a conversao para float
var dec = '12.5';
var decF = parseFloat(dec);
console.log(decF);
console.log(typeof(decF));

// observe que nas saidas do typeof só aparacem 'number'.
// isso ocorre por que o javascript entende todos os tipos numericos como um só!
