// Usando testes condicionais!

/*
  if (condition é verdadeira) {
    código a ser executado!
  }

  o bloco if testa se uma condição é verdadeira. Sendo, ele executa os comandos
*/

function test (myCondition) {
  if (myCondition) {
     return "It was true";
  }
  return "It was false";
}
console.log(test(true)); // returns "It was true"
console.log(test(false)); // returns "It was false"

var idade = 19;
if (idade >= 18) {
  console.log('Retornou pois o teste foi verdadeiro!')
}

var nome = 'Junior';
if (nome == 'Ricardo') {
  console.log('Não será executada!')
}
// a saida acima não será executada, pois o teste resultará em false!

// Operadores de Comparação são usados para realizar os testes!
var a = 5 > 3   // > maior
var b = 3 < 5   // < menor
var c = 3 != 5  // != diferente
var d = 2 == 2  // == igualdade
// podemos testar o >= (maior ou igual) e <= (menor ou igual) também!

var teste1 = 3 == '3';
var teste2 = 3 === '3';
console.log(teste1);
console.log(teste2);

/*
  A igualdade estrita (===) é a contrapartida do operador de igualdade (==).
  No entanto, ao contrário do operador de igualdade, que tenta converter os
  dois valores sendo comparados a um tipo comum, o operador de igualdade
  estrita não executa uma conversão de tipo.
*/
