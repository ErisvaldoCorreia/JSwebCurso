/*
  Em JavaScript, o escopo se refere à visibilidade das variáveis. Variáveis
  definidas fora de um bloco de funções possuem escopo Global. Isso significa
  que eles podem ser vistos em qualquer lugar no seu código JavaScript.

  As variáveis que são usadas sem a palavra-chave var são criadas
  automaticamente no escopo global. Isso pode criar consequências indesejadas
  em outro lugar no seu código ou ao executar uma função novamente.
*/
var myGlobal = 10;

function fun1() {
  var local = 15;
}

console.log(myGlobal);
// Esse trecho gerará um erro na execução - Local is not defined
// console.log(local);

/*
  É possível ter variáveis locais e globais com o mesmo nome. Quando você faz
  isso, a variável local tem precedência sobre a variável global.
*/

var outerWear = "T-Shirt";
function myOutfit() {
  var outerWear = "sweater";
  return outerWear;
}

console.log(myOutfit()); // saida: sweater

// Uma função sem a palavra chave return, executa um grupo de códigos, porém
// possui valor de retorno como Undefined.
var sum = 3;
function addFive() {
  sum = sum + 5;
}

var returnedValue = addFive();
console.log(returnedValue); // saida: Undefined
