// Nested Loops (Laços Aninhados)

// Podemos utilizar laços dentro de laços usando a referencia de um dentro do
// outro. Isso é muito usado para manipulação de matrizes multidimensionais.

function multiplyAll(arr) {
  var product = 1;
  for (var i=0; i < arr.length; i++) {
    for (var j=0; j < arr[i].length; j++) {
      product *= arr[i][j];
    }
  }
  return product;
}

var total = multiplyAll([[1,2],[3,4],[5,6,7]]);
console.log(total);
/*
  No exemplo acima, observamos uma função que recebe um array multidimensional
  Dentro da função usamos um laço for para percorremos a linha do array, enquanto
  usamos o segundo laço para percorremos o que seria as colunas do array.
*/

// Exemplo simples para entendimento:
var matriz = [[0,0,0], [1,1,1], [2,2,2]];
/*
  Com essa declaração estamos criando algo que poderia ser visualizado da
  seguinte forma:
  [ 0 , 0 , 0 ]
  [ 1 , 1 , 1 ]
  [ 2 , 2 , 2 ]
  Em uma forma de impressão em log podemos ter a saida ([0,0,0,1,1,1,2,2,2])
*/
// Percorrendo a linha da matriz
for (var i=0; i < matriz.length; i++) {
  // Percorrendo as colunas da matriz
  for (var j=0; j < matriz[i].length; j++) {
    console.log('Valor: ' + matriz[i][j]);
  }
}

// Perceba que a cada iteração da variavel i, a variavel j é reiniciada.
