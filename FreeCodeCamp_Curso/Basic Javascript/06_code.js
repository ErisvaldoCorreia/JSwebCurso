// Aprofundando os entendimentos em Funções!

/*
  function functionName(parâmetro se houver) {
     códigos;
     return valor 'se houver';
  }

  Uma função é criada usando a palavra chave function seguida de um nome para
  a mesma e parenteses. Caso haja algum parâmetro, ele será colocado dentro
  dos parenteses como uma variavel para ser usada na função. Dentro dela
  podemos realizar os códigos necessários. Caso ela só execute alguma ação,
  não precisa da palavra return. Porém se a mesma tiver de retornar algum valor
  ou mensagem ao objeto que a chamou, devemos colocar em seguida da palavra
  chave return.
*/

// função sem retorno
function retornaVazio() {
  var frase = 'Olá mundo';
  console.log(frase)
}

// chama a função que irá imprimir um valor no console
retornaVazio();

// função com retorno 
function retornaValor(x, y) {
  var soma = x + y;
  return soma;
}

// chama a função e exibe o valor retornado no console
console.log(retornaValor(5,4)); // saida esperada: 9

/*
  Em uma primeira abordagem, pode ser um pouco difícil saber quando algo é
  um parâmetro ou um argumento. A diferença principal está em onde eles
  aparecem no código. Um parâmetro sempre será um nome de uma variável, e ele
  aparece na declaração da função. Por outro lado, um argumento sempre será um
  valor (ou seja, qualquer um dos tipos de dados do JavaScript - do tipo number,
  string, boolean, etc.) e sempre aparecerá no código quando a função é chamada
  ou invocada.
*/
