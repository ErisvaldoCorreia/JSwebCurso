// Usamos a função .push() para enviar novos valores em um array
// Ele coloca os elementos ao final do array após o ultimo indice!

var ourArray = ["Stimpson", "J", "cat"];
ourArray.push(["happy", "joy"]);
// ourArray ficará assim ["Stimpson", "J", "cat", ["happy", "joy"]]
var myArray = [["John", 23], ["cat", 2]];
myArray.push(["dog", 3]);
console.log(myArray);

// Usamos a função .pop() quando queremos remover o ultimo valor de um array
// Ele remove o valor e o devolve. Podemos assim armazena-lo em uma variavel
var ourArray = [1,2,3];
var removedFromOurArray = ourArray.pop();
console.log(removedFromOurArray);
var myArray = [["John", 23], ["cat", 2]];
var removedFromMyArray = myArray.pop();
console.log(removedFromMyArray);

// Usamos a função .shift() igual ao .pop(), com a diferença de que shift
// remove o primeiro item de um array
var ourArray = ["Stimpson", "J", ["cat"]];
var removedFromOurArray = ourArray.shift();
console.log(removedFromOurArray);
var myArray = [["John", 23], ["dog", 3]];
var removedFromMyArray = myArray.shift();
console.log(removedFromMyArray);

// Usamos a função .unshift como a função .push()
// Ela adiciona novos elementos, porém no inicio do array!
var ourArray = ["Stimpson", "J", "cat"];
ourArray.shift(); // ourArray now equals ["J", "cat"]
ourArray.unshift("Happy");
console.log(ourArray);
var myArray = [["John", 23], ["dog", 3]];
myArray.shift();
myArray.unshift(["Paul", 35]);
console.log(myArray);
