// as vezes precisamos fazer dois testes para um determinado codigo!
var idade = 18;
var money = 'sim';

if (idade >= 18) {
  if (money == 'sim') {
    console.log('Será executado!');
  }
}

// podemos evitar esse aninhamento de ifs usando operadores lógicos!
// usamos && (para operações do tipo E)
// usamos || (para operações de comparação tipo OU)

// se idade maior ou igual a 18 E money for igual a 'sim'
if (idade >= 18 && money == 'sim') {
  console.log('Será executado pois ambos os testes foram verdadeiros');
}

// se idade maior ou igual a 18 OU dinheiro igual a 'sim'
var idade = 15;
var money = 'sim';
if (idade >= 18 || money == 'sim') {
  console.log('Será executado pois um dos testes foi verdadeiro');
}

// para saidas em caso retorno do teste seja false podemos usar o else
// else funciona como um caso contrario, tendo uma opção para retornos false
var sol = false;
if (sol) {
  console.log('Esta fazendo sol');
} else {
  console.log('Não esta fazendo sol!');
}

// Nesse exemplo atribuimos um valor false a variavel sol!
// o teste no if espera um valor como true, por isso apenas indicamos a variavel
// por ter um retorno false, o if não é executado mas o bloco else será!

var idade = 14;
if (idade >= 18) {
  console.log('Idade maior que 18');
} else {
  console.log('Idade menor que 18');
}
