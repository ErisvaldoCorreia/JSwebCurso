// Fixando nossos retornos booleanos de forma reduzida.
function isLess(a, b) {
  if (a < b) {
    return true;
  } else {
    return false;
  }
}

var recebe = isLess(10, 15);
console.log(recebe);

// Perceba que podemos encurtar nossa função retirando o teste e fazendo a
// ação diretamente no return com um teste.
function isLessAgain(a, b) {
  return a < b;
}

var recebe2 = isLessAgain(10, 15);
console.log(recebe2);
