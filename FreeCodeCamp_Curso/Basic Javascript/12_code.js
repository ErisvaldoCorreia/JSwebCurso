// Exercicio de revisão
/*
  No jogo de golfe, cada buraco tem um par que significa o número médio de
  golpes que um golfista deve fazer para afundar a bola em um buraco para
  completar a jogada. Dependendo da distância acima ou abaixo dos seus traços,
  há um apelido diferente.

  Sua função receberá argumentos par e strokes. Retorna a string correta de
  acordo com esta tabela que lista os traços em ordem de prioridade;
  topo (maior) para baixo (menor):

  Strokes	Return
  1	"Hole-in-one!"
  <= par - 2	"Eagle"
  par - 1	"Birdie"
  par	"Par"
  par + 1	"Bogey"
  par + 2	"Double Bogey"
  >= par + 3	"Go Home!"
*/

var names = [
  "Hole-in-one!",
  "Eagle",
  "Birdie",
  "Par",
  "Bogey",
  "Double Bogey",
  "Go Home!"
  ];

function golfScore(par, strokes) {
  if (strokes == 1){
    return "Hole-in-one!";
  } else if (strokes <= par -2){
    return "Eagle";
  } else if (strokes == par -1) {
    return "Birdie";
  } else if (strokes == par) {
    return "Par";
  } else if (strokes == par +1) {
    return "Bogey";
  } else if (strokes == par +2) {
    return "Double Bogey";
  } else {
    return "Go Home!";
  }
}

// chamada teste
golfScore(5, 4);
