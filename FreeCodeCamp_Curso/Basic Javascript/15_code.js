// Revendo conceitos
/*
  No jogo de cassino Blackjack, um jogador pode ganhar uma vantagem sobre a
  casa, mantendo o controle do número relativo de cartas altas e baixas
  remanescentes no baralho. Isso é chamado de contagem de cartão.
  Ter mais cartas altas restantes no baralho favorece o jogador. Cada cartão
  recebe um valor de acordo com a tabela abaixo. Quando a contagem é positiva,
  o jogador deve apostar alto. Quando a contagem é zero ou negativa, o jogador
  deve apostar baixo.

  +1 -> 2,3,4,5,6
   0 -> 7,8,9
  -1 -> 10,J,Q,K,A

  Você vai escrever uma função de contagem de cartões. Ele receberá um parâmetro
  de cartão, que pode ser um número ou uma sequência, e incrementará ou
  decrementará a variável de contagem global de acordo com o valor da carta
  (consulte a tabela). A função retornará uma string com a contagem atual e a
  string Bet se a contagem for positiva ou Hold se a contagem for zero ou
  negativa. A contagem atual e a decisão do jogador (Bet ou Hold) devem ser
  separadas por um único espaço.
*/

var count = 0;
function contCard(card) {
  switch(card){
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
      count++;
      break;
    case 10:
    case "J":
    case "Q":
    case "K":
    case "A":
      count--;
      break;
  }
  if (count > 0){
    return count + " Bet";
  } else {
    return count + " Hold";
  }

  return "Change Me";
}

// chamadas da função
contCard(2);
contCard(3);
contCard(7);
contCard('K');
contCard('A');
