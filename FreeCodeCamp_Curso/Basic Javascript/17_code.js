// Loops (iterações)

// laço de repetição enquanto (while)
var contador = 0;
while (contador < 5) {
  console.log(contador);
  contador++;
}

/*
  Podemos ler a instrução acima como:
  Contador recebe valor 0. Enquanto contador for menor que 5 faça o console
  mostrar o valor de contador e some mais 1 na variavel contador.
  Por ser um laço de repetição o codigo será verificado novamente. Enquanto o
  teste retornar verdadeiro os comandos dentro da instrução while será executado.
*/

// laço de repetição para (for)
for (var i = 0; i < 5; i++) {
  console.log(i);
}

/*
  Podemos ler a instrução acima como:
  Para variavel que começa em 0, até que ela seja menor que 5 execute os comandos
  e incremente mais 1 na variavel.
  Podemos perceber que os dois laços fazem basicamente a mesma coisa, porém o
  laço for é mais sucinto e direto em sua forma de escrita.
*/

// usando arrays para percorrer laços.
var array = [0,3,7,8,1];
var total = 0;
for (var i = 0; i < array.length; i++) {
  total += array[i]
}
console.log('Valor somado do array: ' + total);
