// Objetos
// Objetos funcionam como arrays, porém ao invés de acessarmos via indices,
// usamos propriedades nomeadas para acessarmos suas informações. Semelhante
// a um dicionario da linguagem Python.

// forma geral <nome_Variavel> = { "nomePropiedade1":'valorPropriedade1', };
// usamos a virgula dentro das chaves para separarmos outras propriedades.

var ourDog = {
  "name": "Camper",
  "legs": 4,
  "tails": 1,
  "friends": ["everything!"]
};

// note que não temos uma propriedade chamada money aqui declarada!
// abaixo segue explicação do porque dessa observação!
var person = {
  "name": 'Erisvaldo',
  "age": 31,
  "civil state": 'married'
};


// Para acessarmos os valores das propriedades podemos usar um .<nomePropiedade>
// ou usarmos as notações de colchetes [] (usada prioritariamente em nomes de
// propriedades que contenha espaços)

console.log(ourDog.name);
// retorna a propriedade name do objeto ourDog
console.log(person['name']);
// retorna a propriedade name do objeto person
console.log(person['civil state']);
// retorna a propriedade civil state do objeto person

// Podemos alterar o valor de uma propriedade pelo método de atribuição
person['age'] = 40;
console.log(person.age);

// podemos criar novas propriedades nomeando a mesma e atribuindo um valor
person['money'] = 'no';
console.log(person.money);

// podemos também excluir uma propriedade existente ou criada usando o delete
delete ourDog.tails;
