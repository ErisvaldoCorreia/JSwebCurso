// Gerando numeros aleatorios com Javascript e Random()

// Math.random() gera um numero fracional entre 0 e 1
var num = Math.random();
console.log(num);

// Para retornarmos um valor Inteiro podemos usar a junção de uma multiplicação
// com o Math.floor() para retirarmos as casas decimais.

var num = Math.floor(Math.random() * 20);
console.log(num);

// Porém, o valor 20 sempre será mantido de fora. Para que ele ou qualquer outro
// apareça entre os possiveis selecionados podemos ou declarar um numero a mais
// ou fazermos uso de uma soma ao final da multiplicação.

var num = Math.floor(Math.random() * 20 + 1);
console.log(num);

// definindo o inicio e o fim dos valores que poderão ser gerados!
function randomRange(myMin, myMax) {
  return Math.floor(Math.random() * (myMax - myMin + 1)) + myMin;
}

var myRandom = randomRange(5, 15);
console.log(myRandom);
