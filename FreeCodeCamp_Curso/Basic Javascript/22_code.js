// operdaor ternario

// podemos simplificar a forma da escrita do if
var teste = false;
if (teste) {
  console.log('aparece se verdadeiro');
} else {
  console.log('aparece se falso');
}

// podemos escrever essa sintaxe usando uma forma conhecida como operação ternaria
var recebe = teste ? 'aparece se verdadeiro' : 'aparece se falso';
console.log(recebe);

/*
  Nessa forma de escrita usamos o operador ? depois do teste como uma pergunta.
  O valor logo em seguida seria o da resposta true e depois dos : temos o retorno
  caso seja falso. Podemos usar essa estrutura para multiplos testes também!
  <teste> ? <resposta_true> : <resposta_false>;
  <teste1> ? <resposta_true> : <teste2> ? <resposta_true> : <resposta_false>;
*/

var teste = 16;
var recebe = (teste >= 18) ? 'maior de idade' : (teste > 15)? 'maior de 15' : 'menor';
console.log(recebe);
