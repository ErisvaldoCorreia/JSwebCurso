// Usando o switch no lugar de multiplos testes com if

// exemplo:
function caseInSwitch(val) {
  var answer = "";
  switch(val) {
    case 1:
      answer = "alpha";
      break;
    case 2:
      answer = "beta";
      break;
    case 3:
      answer = 'gamma';
      break;
    case 4:
      answer = 'delta';
      break;
    default:
      answer = 'Erro';
      break;
  }
  return answer;
}

caseInSwitch(1);
caseInSwitch(3);
// não temos um teste para o valor 7! Entrará no default
caseInSwitch(7);

// Entendendo a estrutura.
// Usamos o switch para fazer um teste com valores. Ele funciona como um ===
// e testamos os possiveis valores dentro dele com o case. Ao final de cada
// case usamos o break para interromper os testes assim que ele encontra uma
// opção valida!
// Podemos adicionar um default no final que funcionaria como um else caso
// nenhuma das opções seja valida!
