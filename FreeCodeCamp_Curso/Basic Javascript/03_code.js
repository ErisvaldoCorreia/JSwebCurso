// Praticando estudos de string com proposta "Mad Libs Game"

// Iremos usar uma função que irá receber 4 palavras em suas variaveis locais
// elas serão usadas para compor uma frase completa.
// usamos a palavra function para declarar uma função e return para o retorno
// do valor ao objeto chamador da função!
function wordBlanks(myNoun, myAdjective, myVerb, myAdverb) {
  // Your code below this line
  var result = "One " + myNoun + " better " + myAdjective;
  result += " " + myVerb + " much " + myAdverb;
  // Your code above this line
  return result;
}

// Change the words here to test your function
var frase = wordBlanks("dog", "big", "ran", "quickly");
console.log(frase);
