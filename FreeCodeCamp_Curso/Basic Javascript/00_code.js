// exemplo de comentário de uma linha

/*
exemplo de comentário de multilinhas
apropriado em documentação do código
*/

// declaração de variaveis
var nomeVariavel;

// declaração com inicialização em conjunto
var nomePessoa = 'Erisvaldo Correia';

// inicialização de variavel declarada
nomeVariavel = 'Inicializando';

// usando o log do console para visualização de resultados
console.log(nomeVariavel);
console.log(nomePessoa);

// se usarmos aspas nos parametros do log, estaremos informando uma string
console.log('Imprimindo mensagem direta');

// operações com variaveis e mostra de resultados
var a = 5;
var b = 10;
var c = 'I am a';

a = a + 10;
b = b + a;
c = c + " String!";

// concatenando uma variavel a uma string usando o operador +
console.log('Resultado de A = ' + a);
console.log('Resultado de B = ' + b);
console.log(c);
