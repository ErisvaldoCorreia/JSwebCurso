/*
    Treinamento em Javascript
    Udacity 2018
    Erisvaldo Correia
*/

// temos os tipos Null e undefined.
// null refere-se ao "valor de nada"
// retorna null  na saida do console
var x = null;
console.log(x);

// undefined refere-se à "ausência de valor"
// retorna undefined na saida do console
var signedIn;
console.log(signedIn);

// temos também o retorno de NaN (Not a Number)
// isso ocorre quando temos uma falha na operação aritmética
console.log('hello' / 5);
console.log(Math.sqrt(-10));

// Math.sqrt é uma chamada para calcularmos Raiz Quadrada.

/* Javascript é uma linguagem Fracamente Tipada, ou seja, não são precisos
 * criarmos processos de declaração de tipos como em algumas linguagens.
 * Em Javascript podemos concatenar uma string com um valor dado inteiro,
 * pois será feita a conversão e adequação dos tipos pela "Coerção de tipos
 * Implicita".
 */

// usamos o operador === para comparação de igualdade estrita
// retorna true na saida do console (igualdade em tipo e valor)
console.log('3' === '3');

// retorna false na saida do console (igualdade em valor apenas)
console.log('1' === 1);
