/*
    Treinamento em Javascript
    Udacity 2018
    Erisvaldo Correia
*/

// Funções - Blocos de código reusaveis!
// damos um nome para nossa função e podemos passar parametros.
function nome(param1, param2) {
  console.log(param1, param2);
}

nome('Erisvaldo', 'Junior');

// O uso das funções permite um melhor aproveitamento do nosso código!
// Podemos fazer manutenções que serão aplicadas em todas as chamadas da função.

// Vejamos um exemplo de reuso de função.
function someNum(a, b) {
  return a + b;
}

console.log(someNum(3,4));
console.log(someNum(9,2));
