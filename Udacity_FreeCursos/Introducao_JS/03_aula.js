/*
    Treinamento em Javascript
    Udacity 2018
    Erisvaldo Correia
*/

// Booleanos são tipos de dados que só aceitam dois valores:
// True ou False (Verdadeiro ou Falso)

// irá armazenar o retorno booleano da expressão
var resultado1 = 5 > 3;
var resultado2 = 4 != 4;

// irá retornar True na saida do console
console.log(resultado1);

// irá retornar False na saida do console
console.log(resultado2);

// em todos os testes de comparação, os retornos sempre serão Booleanos
/* Exemplo de aplicação com uso condicional */
var idade = 19;

if (idade >= 18) {
    console.log('Idade maior do que 18');
} else {
    console.log('Idade menor do que 18');
}

// no caso desse teste condicional, apenas será executado um retorno.
// o bloco dentro if será executado se o retorno for True
// o bloco dentro do else será executado se o retorno for False

// Podemos fazer multiplos testes usando o else if como testes!

var nota = 6;

if (nota > 8) {
  console.log('aprovado');
} else if (nota > 5) {
  console.log('recuperação');
} else {
  console.log('reprovado');
}

// perceba no exemplo acima que o valor da variavel nota é 6. Este é maior que 5,
// e menor que 8. portanto será impresso o valor de recuperação.
