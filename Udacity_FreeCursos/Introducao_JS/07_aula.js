/*
    Treinamento em Javascript
    Udacity 2018
    Erisvaldo Correia
*/

// Funções são blocos reutilizaveis de codigo.
// Com elas podemos escrever uma sequencia de comandos qual podemos usar em
// varias partes do nosso programa sempre que necessario. Isso organiza nosso
// codigo, bem como torna o mesmo mais legivel e facil de se fazer manutenção.

// criando um função que simplesmente imprime uma mensagem!
function imprimeOla() {
  console.log('Olá mundo!');
}

// chamando essa função
imprimeOla();


function maisCode() {
  console.log('Imprimindo uma linha');
  console.log('Imprimindo segunda linha');
  console.log('Imprimindo terceira linha');
}

// imagine um codigo que tivesse que repetir esses comandos logs varias vezes!
// o fato de estar em uma função facilita nossa manutenção visto que apenas
// precisamos agora chamar o a função. Caso precisemos alterar algo, fazemos isso
// uma unica vez na função que todos os locais que chamam ela, receberão essa
// modificação!

maisCode();
maisCode();
