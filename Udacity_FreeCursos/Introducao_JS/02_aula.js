/*
    Treinamento em Javascript
    Udacity 2018
    Erisvaldo Correia
*/

// Javascript é case sensitive (diferência letras maiuscula e minusculas)
// ou seja, YES, Yes, yes, YeS são diferentes.

// retorna False na saida do console
console.log('Yes' == 'YES');
// retorna True na saida do console
console.log('yes' == 'yes');
// retorna False na saida do console
console.log('YES' != 'YES');

// O Javascript realiza a comparação entre caracteres usando a tabela ASCII
// por isso comparações como essas, retornam como sendo True
console.log('green' > 'blue');
console.log('green' > 'Green');

// Caracteres especiais:
// \n (pular linha) - \t (tabular) - \\ (uso de barra invertida)

// Exercicio proposto.
/*
 * Programming Quiz: All Tied Up (2-5)
 * Crie uma frase com pergunta e respota cada uma em uma linha
 */

var joke = 'O que o cravo disse para a rosa? \nNada, cravo não fala'
console.log(joke);
