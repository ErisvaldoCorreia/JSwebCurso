/*
    Treinamento em Javascript
    Udacity 2018
    Erisvaldo Correia
*/

// usando switch para escolhas

var escolha = 3;
switch (escolha) {
  case 1:
    console.log('escolheu 1');
    break;
  case 2:
    console.log('escolheu 2');
    break;
  case 3:
    console.log('escolheu 3');
    break;
  case 4:
    console.log('escolheu 4');
    break;
  default:
    console.log('escolheu uma opçao invalida');
    break;
}

// esse bloco de comandos funciona como um if com varios testes baseados em uma
// variavel de teste. ele funciona como um teste de equidade no qual o valor
// da variavel deve ser exatamente igual ao presente nos cases
