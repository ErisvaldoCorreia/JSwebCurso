/*
    Treinamento em Javascript
    Udacity 2018
    Erisvaldo Correia
*/

// Executando códigos diversas vezes!
// Laço while - imprimindo valores de 1 a 5
var cont = 1;
while (cont < 6) {
  console.log('variavel em: ' + cont);
  cont = cont + 1;
}

// Nesse caso iniciamos uma variavel chamada cont com o valor 1.
// O while testa se ela é menor que 6. Sendo positivo imprime um console
// Em seguida acrescenta 1 ao valor da variavel. E reexecuta o teste!

// Podemos também utilizar o laço for!

for (var i = 1; i < 6; i++) {
  console.log('o valor esta em' + i);
}

// perceba que no caso do for, também inicializamos uma variavel contadora.
// nesse caso usamos a letra i. Fazemos em sequencia o teste e posteriormente o
// acrsscimo. Caso os testes sejam verdadeiros, o comando dentro do for é executado.

// no caso do while podemos garantir pelo menos uma execução
var teste = 1;
do {
  console.log('teste em: ' + teste);
  teste = teste + 1;
} while (teste < 6);
