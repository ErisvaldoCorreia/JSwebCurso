/*
    Treinamento em Javascript
    Udacity 2018
    Erisvaldo Correia
*/

//  exibe uma mensagem no terminal de execução
console.log("Bem vindo ao Curso de Javascript")


//  operações matemáticas e operadores de comparação
//  (* / + -) (> >= < <= == !=)
//  retorna 6 como resposta
console.log(3 + 5 - 2)
//  retorna false (falso) como resposta
console.log(3 + 5 < 4 -1)


/*
  Função exemplo para explicação de conceitos
  Função que inverte uma palavra e exibe no console
  word - variavel que recebe a palavra
  na chamada da função
*/

function reverse(word) {
  // declarando a variavel que recebera a palavra invertida
  var reverseword = "";
  // loop que começa com o tamanho da palavra
  // corre em passo 1 regressivo
  for (var i = word.length - 1; i>=0; i--) {
    // adiciona a letra correspondente ao loop e concatena
    // a palavra final revertida
    reverseword = reverseword.concat(word[i]);
  }
  // retorna a palavra invertida para quem chamou a função
  return reverseword;
}
// chamada da função e exibindo a palavra invertida
// saída esperada ("olleH")
console.log(reverse("Hello"))
