/*
    Treinamento em Javascript
    Udacity 2018
    Erisvaldo Correia
*/

// Objetos.
var obejto = {
  nome: 'Erisvaldo',
  apelido: 'Junior'
};
// usamos como primeiro termo a referencia separados por : para o valor a ser armazenado.
console.log(objeto);

// Objetos são conjuntos de dados como um array, porém que associamos a uma key
// Exemplo: podemos chamar o valor do nome passando como referencia do objeto.
console.log(objeto.nome);

// Podemos também ter objetos dentro de objetos.
var funcionario = {
  nome: 'Junior',
  cargo: 'programador',
  empresa: {
    nome: 'Udacity',
    entrada: '10/02/2018',
    saida: ''
  }
};

// Caso queiramos acessar o nome da empresa, passamos a referencia encadeada.
console.log(funcionario.empresa.nome);
