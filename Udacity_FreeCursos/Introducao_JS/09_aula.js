/*
    Treinamento em Javascript
    Udacity 2018
    Erisvaldo Correia
*/

// Arrays = conjunto de dados multiplos e uma variavel de varios indices
// Sempre começam no indice 0.

var array = ['primeiro', 'segundo', 'terceiro'];
// Neste exemplo temos um array de strings contendo 3 valores.
// A identificação dos mesmos se da de forma ao indice 0, 1 e 2.

console.log(array[1]);
// Passamos o indice dentro de colchetes para determinar qual valor será exibido
// Nesse caso temos como resposta o valor do indice 1, que esta na segunda posição.

/* ----- Métodos de Arrays ------ */
// length - Retorna o tamanho.
console.log(array.length);

// push - Coloca um novo elemento no array ao final dos anteriores.
array.push('quarto');
console.log(array);

// unshift - Coloca um nomo elemento ao começo do Array primordialmente.
array.unshift('novo inicial');
console.log(array);

// pop - Remove o ultimo item do array. Podemos guardalo em uma variavel.
var removeFim = array.pop();
console.log(array);
console.log(removeFim);

// shift - Remove o primeiro item do array. Também podemos guardar em variavel.
var inicio = array.shift();
console.log(array);
console.log(inicio);
