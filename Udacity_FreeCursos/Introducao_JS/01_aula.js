/*
    Treinamento em Javascript
    Udacity 2018
    Erisvaldo Correia
*/

// Strings (sequencia de caracteres)
// devem estar contidas dentro de aspas simples ou duplas.
// deve retornar a frase 'olá mundo' na saida do console.
console.log('olá mundo');
// podemos realizar concatenação de strings usando o operador +
// deve retornar 'ola mundo' na saida do console.
console.log('olá ' + 'mundo');
// o Javascript possui também a questão de conversão.
// podemos somar uma string ao resultado de uma operação.
// o interpretador irá realizar a operação e converter o resultado em string
// deve retornar 'Olá Mundo50' na saida do console.
console.log('Olá Mundo' + 5 * 10);


/* variaveis - armazenamento de valores */
// como em outras linguagens, podemos usar variaveis no Javascript
// para declararmos uma variavel usamos a seguinte estrutura:
// var nomeDaVariavel = valorQueElaRecebe;
var nome = 'Erisvaldo';
var idade = 31;
// deve retornar 'Erisvaldo tem 31 anos' na saida do console
console.log(nome + ' tem ' + idade + ' anos');


// Resolução de Exercicios proposto
/*
 * Programming Quiz: Converting Tempatures (2-2)
 *
 * The Celsius-to-Fahrenheit formula:
 *
 *    F = C x 1.8 + 32
 *
 * 1. Set the fahrenheit variable to the correct value using the celsius
 *    variable and the forumla above
 * 2. Log the fahrenheit variable to the console
 *
 */
// Declarando a variavel de Graus Celsius
var celsius = 12;
// convert celsius to fahrenheit here
var fahrenheit = celsius * 1.8 + 32;
//deve retornar 53.6 na saida do console
console.log(fahrenheit);
