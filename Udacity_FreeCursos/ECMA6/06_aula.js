/*
  Arrows Functions
  O ES6 introduz um novo tipo de função chamada arrow. As funções arrow são
  muito similares a funções regulares em termos de comportamento, mas são
  muito diferentes em termos de sintaxe.
  As arrows são mais sucintas e dinmaicas.
*/

const arr = [100,200,350,420];

function getNum(a) {
  return a[2];
};

console.log(getNum(arr));

// usando a arrow
const getNum2 = (a) => { return a[1] } ;
console.log(getNum2(arr));

/*
  Funções regulares podem ser tanto declaração de funções como expressões de funções, no entanto, 
  funções arrow são sempre expressões. Na verdade, o nome completo da função arrow é "arrow 
  function expressions", e ela só pode ser utilizada onde uma expressão é válida. Isso inclui:

  > estar armazenada em uma variável,
  > ser passada como argumento de uma função,
  > e armazenada na propriedade de um objeto.

  A lista de parâmetros aparece antes da flecha da arrow (=>). Caso essa lista só possua um 
  parâmetro, você pode escrevê-la como no exemplo acima, omitindo os parênteses. No entanto, se 
  houver dois ou mais itens na lista de parâmetros ou mesmo não houver nenhum parâmetro na lista, 
  você precisará envolver a lista em parênteses.
*/

// arrow com apenas um parametro inline
const greet = name => `Hello ${name}!`;


// lista de parâmetros vazia exige parênteses
const sayHi = () => console.log('Hello Udacity Student!');


// múltiplos parâmetros na lista, parênteses obrigatórios!
const orderIceCream = (flavor, cone) => 
  console.log(`Here's your ${flavor} ice cream in a ${cone} cone.`);

// arrows block com uso de chaves
const blockSum = (num1, num2) => {
  let somar = 0;
  somar = num1 + num2;
  return somar;
};


// chamadas das arrows exemplos.
greet('Junior');
sayHi();
orderIceCream('chocolate', 'waffle');
blockSum(10, 15);
