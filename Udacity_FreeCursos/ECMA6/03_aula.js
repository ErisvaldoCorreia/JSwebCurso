/*
Exercicio prático para fixação:

Use array destructuring para retirar o valor de três variáveis cores e
armazene cada um deles nas variáveis one, two e three.
*/

const things = [
  'red', 'basketball', 'paperclip',
  'green', 'computer', 'earth',
  'udacity', 'blue', 'dogs'
];

const [one,,, two,,,, three,] = things;

const colors = `List of Colors
1. ${one}
2. ${two}
3. ${three}`;

console.log(colors);
