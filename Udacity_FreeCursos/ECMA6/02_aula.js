/*
Usando o destructuring.
Em ES6, podemos coletar dados de um Array para multiplas variaveis usando o
método de desestruturação.
*/

// Modo Antigo
const valores = [10, 25, 40];
const a = valores[0];
const b = valores[1];
const c = valores[2];

console.log(a, b, c);

// desestruturação
const [num1, num2, num3] = valores;
console.log(num1, num2, num3);

// Para saltar um valor que nao deseja coletar, basta adicionar um espaço vazio
const [valor1, , valor3] = valores;
console.log(valor1, valor3);

// Podemos coletar valores de Objetos também a apartir de variaveis identificaforas
const objeto = {
  nome : 'Junior',
  idade : 32,
  cargo : 'programador'
};

const {nome, cargo} = objeto;
console.log(nome, cargo);

// Note que usamos uma referencia ao objeto com as variaveis com mesmo nome.
// Isso nos permite coletar os dados, mesmo fora de ordem!
