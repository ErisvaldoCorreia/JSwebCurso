/*
  for ... of loops
  Introduzido no modelo ES6, essa forma de trabalhar as iterações possui como
  caracteristica a sua forma enxuta e direta de trabalhar, bem como a eliminação
  do uso dos indices de referências.
*/

const array = [1,2,3,4,5,6,7,8,9];


// método tradicional de iterar
for (var i = 0; i < array.length; i++) {
  console.log(array[i]);
};


// usando o for of - note que não precisamos de indices e contadores
for (arr of array) {
  console.log(arr);
};


// Mais um exemplo
const days = [
  'sunday',
  'monday',
  'tuesday',
  'wednesday',
  'thursday',
  'friday',
  'saturday'
];


for (day of days) {
    console.log(day);
}
