/*
  Rest / Spread
  O operador spread, escrito com 3 pontos consecutivos ( ... ), é novo no ES6
  e nos permite separar o spread iterable objects (objetos iteráveis) em
  múltiplos elementos.
  Se você pode utilizar o operador spread em diversos elementos, então,
  certamente deve haver uma forma de agrupar diversos elementos na array
  novamente, certo? Sim esse é o formato do parametro rest
  Outro caso de uso para o parâmetro rest é quando se está trabalhando com
  funções variadic. Funções variadic são funções que podem receber um número
  indefinido de argumentos.
*/

// SPREAD
const fruits = ["apples", "bananas", "pears"];
const vegetables = ["corn", "potatoes", "carrots"];
const produce = [...fruits, ...vegetables];
console.log(produce);

const num1 = [1,2,3,4];
const num2 = ['a','b',...num1,'c','d'];
console.log(num2);


// REST
function sum(...nums) {
  let total = 0;
  for(const num of nums) {
    total += num;
  }
  return total;
}

console.log(sum(2,6,8,4));
