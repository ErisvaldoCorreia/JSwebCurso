/*
Templete literals:
Ao invés de ficarmos passando diversas concatenações no momento de compor
uma string usando o operador (+), podemos agora construir strings usando (``)
Isso permite que passemos referencias de variaveis e objetos diretamente dentro
da string final a ser construida.
*/

const nome = 'Carlos';
const frase = `Eu fui a praia com o ${nome}`;
console.log(frase);

/*
Usamos a expressão ${variavel} para indicar o objeto ou referencia que iremos
usar na string formada. Com as templates literals fica muito mais facil também
fazer saidas de multiplas linhas!
*/

const words = {
  nome : 'junior',
  idade : 38,
  cargo : 'programador'
};

// Note que não é preciso usar o \n para quebra de linhas!
const string = `O novo funcionario da empresa
se chama ${words.nome}, ele tem ${words.idade} de idade.
Sua Função será de ${words.cargo}`;

console.log(string);
