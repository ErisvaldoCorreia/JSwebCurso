/*
Uso do let e const no lugar de var
as palavras let e const foram introduzidas para evitar possiveis erros
durante a execução do nosso codigo onde antes usavamos a palavra var.

Antes, quando usávamos var, variáveis possuíam o escopo global ou o escopo
local de uma função inteira. let e const possuem seu escopo dentro do bloco
onde são declaradas.
Se uma variável é declarada usando let ou const no interior de um bloco de
código (delimitado por chaves { }), então a variável fica presa no que chamamos
de zona morta temporal até que a declaração da variável seja processada.
Esse comportamento garante que as variáveis só possam ser acessadas após
a sua declaração.
*/

let geral = 'variavel vista no codigo!'; // vista no codigo todo!

function testes(cond) {
  if (cond) {
    let local1 = 'pertence somente ao bloco if';
    console.log(local1);
    console.log(geral);
  } else {
    let local2 = 'pertence somente ao bloco else';
    console.log(local1); // irá retornar um erro!
    console.log(geral);
  }
}

// se trocarmos o parametro true por false, iremos obter um erro!
// isso porque a variavel local1 esta definida somente dentro do if
testes(true);
